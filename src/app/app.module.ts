import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductosService } from './_services/productos.service';
import { HttpClientModule } from '@angular/common/http';


import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { LayaoutComponent } from './layaout/layaout.component';
import { SiderComponent } from './layaout/sider/sider.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductsComponent } from './products/products.component';
import { NewProductComponent } from './products/new-product/new-product.component';
registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    LayaoutComponent,
    SiderComponent,
    DashboardComponent,
    ProductsComponent,
    NewProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgZorroAntdModule,
    FormsModule,
  ],
  providers: [ProductosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
