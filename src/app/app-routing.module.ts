import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayaoutComponent } from './layaout/layaout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductsComponent } from './products/products.component';
import { NewProductComponent } from './products/new-product/new-product.component';

const routes: Routes = [
  {
    path: '',
    component: LayaoutComponent,
    children: [
      {
        path : '',
        component: DashboardComponent
      },
      {
        path: 'products',
        component: ProductsComponent
      },
      {
        path: 'products/new-product',
        component: NewProductComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
