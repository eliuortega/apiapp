import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  Header = 'To Do List';
  Footer = 'End';
  data = [
    'Minimo Stock',
    'total de productos',
    'ultimos agregados',
    'total de ingreso de mercancia'
  ];
  constructor() { }

  ngOnInit() {
  }

}
