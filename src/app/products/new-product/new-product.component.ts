import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../../_services/productos.service';
import { IProductos } from '../../shared/Productos.model';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {
  name: string;
  description: string;
  costo: number;
  stock: number;
  productos: IProductos;

  constructor(public producto: ProductosService) { }

  ngOnInit() {
  }

  PostForm() {
    this.productos = {
      nombre: 'nombre',
      descripcion: 'descripcion',
      precioUnidad: 5,
      stock: 4
    };
    this.producto.postProduct(this.productos).subscribe(data => {
      console.log(data);
    });
  }

}
