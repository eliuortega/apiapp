import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../_services/productos.service';
import IProductos from '../shared/Productos.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  title = 'ApiApp';
  productos: IProductos[];
  constructor(public ver: ProductosService) { }

  ngOnInit() {
    this.imprimir();
  }

  imprimir() {
    this.ver.getProducts().subscribe(data => {
      this.productos = data;
    }, error => console.log(error));
    // console.log(this.productos);
  }

}
