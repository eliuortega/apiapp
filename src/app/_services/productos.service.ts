import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import {  Response } from "@angular/http";
import {Observable} from 'rxjs';
// import 'rxjs/add/operator/map';
import {IProductos} from '../shared/Productos.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  BaseUrl = environment.BaseUrl;
  url = this.BaseUrl + 'Productos/';
  datos: IProductos[];
  body: any;
  constructor(private http: HttpClient) { }

  getProducts () {
    // return this._httpprovider.httpReq('http://192.168.1.40:5000/getmapmasterstore','GET',null,null).map(res =>{<any[]>res.json()}
    return this.http.get<IProductos[]>(this.url);
  }

  postProduct(body) {

    return this.http.post<IProductos>(this.url, body);
  }
}

/*
this.body = {
      Nombre: nombre,
      Descripcion: descripcion,
      PrecioUnidad: costo,
      Stock: stock
    };


*/
