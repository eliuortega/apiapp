export interface IProductos {
  id?: number;
  nombre: string;
  descripcion: string;
  precioUnidad: number;
  stock: number;
}
