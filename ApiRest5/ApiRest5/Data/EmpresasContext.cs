﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ApiRest5.Models
{
    public class EmpresasContext : DbContext
    {
        public EmpresasContext (DbContextOptions<EmpresasContext> options)
            : base(options)
        {
        }

        public DbSet<ApiRest5.Models.Productos> Productos { get; set; }
    }
}
